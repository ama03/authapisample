using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BrockAllen.IdentityReboot;
using BrockAllen.IdentityReboot.Ef;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Auth.Sample.Api.Entities
{
    public class ApplicationUser : IdentityUser, ITwoFactorCode
    {
        // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(IdentityRebootUserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string HashedTwoFactorAuthCode { get; set; }
        public DateTime? DateTwoFactorAuthCodeIssued { get; set; }
    }
}