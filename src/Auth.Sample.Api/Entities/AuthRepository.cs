﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Auth.Sample.Api.Models;
using BrockAllen.IdentityReboot;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Auth.Sample.Api.Entities
{

    public class AuthRepository : IDisposable
    {
        private readonly AuthContext ctx;

        internal readonly IdentityRebootUserManager<ApplicationUser> UserManager;

        public AuthRepository(AuthContext ctx, IdentityRebootUserManager<ApplicationUser> userManager)
        {
            this.ctx = ctx;
            UserManager = userManager;
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel, string role="user")
        {
            var roleClaim = new Claim(ClaimTypes.Role, role);

            var user = new ApplicationUser
            {
                UserName = userModel.Email,
                Email = userModel.Email,
            };
            var result = await UserManager.CreateAsync(user, userModel.Password);
            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await UserManager.FindAsync(userName, password);

            return user;
        }

        public Client FindClient(string clientId)
        {
            var client = ctx.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

           var existingToken = ctx.RefreshTokens.SingleOrDefault(r 
                            => r.Subject == token.Subject && r.ClientId == token.ClientId);

           if (existingToken != null)
           {
             var result = await RemoveRefreshToken(existingToken);
           }
          
            ctx.RefreshTokens.Add(token);

            return await ctx.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
           var refreshToken = await ctx.RefreshTokens.FindAsync(refreshTokenId);

           if (refreshToken != null) {
               ctx.RefreshTokens.Remove(refreshToken);
               return await ctx.SaveChangesAsync() > 0;
           }

           return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            ctx.RefreshTokens.Remove(refreshToken);
             return await ctx.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await ctx.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
             return  ctx.RefreshTokens.ToList();
        }

        public async Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            IdentityUser user = await UserManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            var result = await UserManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await UserManager.AddLoginAsync(userId, login);

            return result;
        }

        public void Dispose()
        {
            ctx.Dispose();
            UserManager.Dispose();

        }
    }
}