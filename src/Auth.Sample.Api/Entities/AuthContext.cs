﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Auth.Sample.Api.Entities
{
    public class AuthContext : IdentityDbContext<ApplicationUser>
    {
        public AuthContext()
            : base("AuthContext")
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}

