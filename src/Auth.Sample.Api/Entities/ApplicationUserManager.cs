using System;
using Auth.Sample.Api.Providers;
using Auth.Sample.Api.Services;
using BrockAllen.IdentityReboot;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Auth.Sample.Api.Entities
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : IdentityRebootUserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store
            , IdentityFactoryOptions<ApplicationUserManager> options)
            : base(store)
        {
            //var manager = new ApplicationUserManager(new IdentityRebootUserStore<ApplicationUser>(context.Get<AuthContext>()));
            // Configure validation logic for usernames
            this.UserValidator = new UserValidator<ApplicationUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = true;
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            this.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            //manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            //{
            //    MessageFormat = "Your security code is {0}"
            //});
            this.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            this.RegisterTwoFactorProvider("Phone Code", new MobileStoredTwoFactorCodeProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            this.EmailService = new EmailService();
            this.SmsService = new SmsService();

            var dataProtectionProvider = new MachineKeyProtectionProvider();// options.DataProtectionProvider;
            this.UserTokenProvider =
                new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("EmailConfirmation"));
        }
    }
}