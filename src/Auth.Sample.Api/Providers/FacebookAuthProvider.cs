﻿using Microsoft.Owin.Security.Facebook;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Auth.Sample.Api.Providers
{
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        public override Task Authenticated(FacebookAuthenticatedContext context)
        {
           
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            foreach (var claim in context.User)
            {
                var claimType = $"urn:facebook:{claim.Key}";
                var claimValue = claim.Value.ToString();
                if (claim.Key.Equals("name", StringComparison.CurrentCultureIgnoreCase))
                {
                    string firstName;
                    var lastName = string.Empty;
                    if (!claimValue.Contains(" "))
                    {
                        firstName = claimValue;
                    }
                    else
                    {
                        firstName = claimValue.Substring(0, claimValue.IndexOf(" ", StringComparison.Ordinal));
                        lastName = claimValue.Substring(claimValue.IndexOf(" ", StringComparison.Ordinal) + 1);
                    }

                    context.Identity.AddClaim(new Claim(ClaimTypes.Surname, firstName));
                    context.Identity.AddClaim(new Claim(ClaimTypes.GivenName, lastName));
                }
                if (!context.Identity.HasClaim(claimType, claimValue))
                {
                    context.Identity.AddClaim(
                            new Claim(claimType, claimValue, "XmlSchemaString", "Facebook"));
                }

            }
            return Task.FromResult<object>(null);
        }

        
    }
}