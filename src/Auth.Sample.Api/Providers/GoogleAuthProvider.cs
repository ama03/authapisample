﻿using Microsoft.Owin.Security.Google;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace Auth.Sample.Api.Providers
{
    public class GoogleAuthProvider : IGoogleOAuth2AuthenticationProvider
    {
        public void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            context.Response.Redirect(context.RedirectUri);
        }

        public Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            var userDetail = context.User;
            context.Identity.AddClaim(new Claim(ClaimTypes.GivenName, context.Identity.FindFirstValue(ClaimTypes.GivenName)));
            context.Identity.AddClaim(new Claim(ClaimTypes.Surname, context.Identity.FindFirstValue(ClaimTypes.Surname)));
            context.Identity.AddClaim(new Claim(ClaimTypes.Email, context.Identity.FindFirstValue(ClaimTypes.Email)));
            context.Identity.AddClaim(new Claim(ClaimTypes.Gender, userDetail.Value<string>("gender")));

            return Task.FromResult<object>(null);
        }

        public Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }
    }
}