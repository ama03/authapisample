using Microsoft.Owin.Security.DataProtection;

namespace Auth.Sample.Api.Providers
{
    public class MachineKeyProtectionProvider : IDataProtectionProvider
    {
        public IDataProtector Create(params string[] purposes)
        {
            return new MachineKeyDataProtector(purposes);
        }
    }
}