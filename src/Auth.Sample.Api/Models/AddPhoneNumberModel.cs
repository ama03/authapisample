﻿using System.ComponentModel.DataAnnotations;

namespace Auth.Sample.Api.Models
{
    public class AddPhoneNumberModel
    {
        [Required]
        [Phone]
        public string Number { get; set; }
    }
}