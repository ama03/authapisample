using System;
using System.ComponentModel.DataAnnotations;

namespace Auth.Sample.Api.Models
{
    public class RegisterExternalBindingModel
    {
        [Required]
        public string CountryIso { get; set; }
        
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        
        public string Gender { get; set; }
        
        public DateTime Birthdate { get; set; }

        [Required]
        public string Provider { get; set; }

        [Required]
        public string ExternalAccessToken { get; set; }
    }
}