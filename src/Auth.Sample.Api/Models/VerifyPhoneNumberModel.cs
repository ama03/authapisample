﻿using System.ComponentModel.DataAnnotations;

namespace Auth.Sample.Api.Models
{
    public class VerifyPhoneNumberModel
    {
        [Required]
        public string Code { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}