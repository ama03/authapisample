﻿namespace Auth.Sample.Api.Models
{
    public class SetPasswordModel
    {
        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }
    }
}