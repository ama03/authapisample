using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace Auth.Sample.Api.Models
{
    public class ExternalLoginDataModel
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string ExternalAccessToken { get; set; }

        public static ExternalLoginDataModel FromIdentity(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            var providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

            if (providerKeyClaim == null || string.IsNullOrEmpty(providerKeyClaim.Issuer) ||
                string.IsNullOrEmpty(providerKeyClaim.Value))
            {
                return null;
            }

            if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
            {
                return null;
            }

            return new ExternalLoginDataModel
            {
                LoginProvider = providerKeyClaim.Issuer,
                ProviderKey = providerKeyClaim.Value,
                FirstName = identity.FindFirstValue(ClaimTypes.Surname),
                Gender = identity.FindFirstValue(ClaimTypes.Gender),
                LastName = identity.FindFirstValue(ClaimTypes.GivenName),
                Email = identity.FindFirstValue(ClaimTypes.Email),
                ExternalAccessToken = identity.FindFirstValue("ExternalAccessToken")
            };
        }
    }

    public class ExternalLoginModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }
}