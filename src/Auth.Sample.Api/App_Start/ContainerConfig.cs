﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using AddinX.Configuration.Contract;
using AddinX.Configuration.Implementation;
using AddinX.Logging;
using Auth.Sample.Api.Entities;
using Autofac;
using Autofac.Integration.WebApi;
using BrockAllen.IdentityReboot;
using BrockAllen.IdentityReboot.Ef;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using RegistrationExtensions = WebApiContrib.IoC.Autofac.RegistrationExtensions;

namespace Auth.Sample.Api
{
    public static class ContainerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Auotfac part
            var builder = new ContainerBuilder();
            builder.RegisterInstance<ILogger>(new SerilogLogger());
            builder.RegisterInstance<IConfigurationManager>(new ConfigurationManagerWrapper());
            builder.Register(c => HttpContext.Current).InstancePerRequest();

            builder.RegisterType<AuthContext>().AsSelf();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication);
            builder.Register<IUserStore<ApplicationUser>>(
                c => new IdentityRebootUserStore<ApplicationUser>(new AuthContext()))
                .AsSelf().InstancePerLifetimeScope();

            builder.Register(
                c => new IdentityFactoryOptions<ApplicationUserManager>
                {
                    DataProtectionProvider =
                        new DpapiDataProtectionProvider("ApplicationName")
                });

            builder.RegisterType<AuthRepository>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>()
                .As<IdentityRebootUserManager<ApplicationUser>>()
                .As<ApplicationUserManager>()
                .InstancePerLifetimeScope();

            // Register your Web API controllers.
            RegistrationExtensions.RegisterApiControllers(builder, Assembly.GetExecutingAssembly());

            // Support WebAPI
            // Set the dependency resolver to be Autofac.
            ApiContext.Container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(ApiContext.Container);
        }
    }
}