using AddinX.Configuration.Contract;
using Autofac;

namespace Auth.Sample.Api
{
    internal static class ApiContext
    {
        private static ApiConfig config;

        internal static IContainer Container { get; set; }

        internal static ApiConfig Config
        {
            get
            {
                if (config != null) return config;
                using (var scope = Container.BeginLifetimeScope())
                {
                    var configManager = scope.Resolve<IConfigurationManager>();
                    config = new ApiConfig(configManager);
                }
                return config;
            }
        }
    }
}