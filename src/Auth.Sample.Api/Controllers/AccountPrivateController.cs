﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AddinX.Configuration.Contract;
using AddinX.Logging;
using Auth.Sample.Api.Entities;
using Auth.Sample.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace Auth.Sample.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/AccountPrivate")]
    public class AccountPrivateController : BaseApiController
    {
        private readonly AuthRepository repo;

        public AccountPrivateController(AuthRepository repo, ILogger logger)
            : base(logger)
        {
            this.repo = repo;
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> AddPhoneNumber([FromBody] AddPhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // Generate the token and send it
            var code = await repo.UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (repo.UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await repo.UserManager.SmsService.SendAsync(message);
            }
            return Ok(new { PhoneNumber = model.Number });
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> EnableTwoFactorAuthentication()
        {
            await repo.UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await repo.UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return Ok();
        }

        [HttpPost]
        public async Task<IHttpActionResult> DisableTwoFactorAuthentication()
        {
            await repo.UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await repo.UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return Ok();
        }

        [HttpPost]
        public async Task<IHttpActionResult> VerifyPhoneNumber(VerifyPhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }
            var result = await repo.UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await repo.UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return Ok(new { Message = "AddPhoneSuccess" });
            }
            // If we got this far, something failed
            return StatusCode(HttpStatusCode.NotFound);
        }

        [HttpPost]
        public async Task<IHttpActionResult> RemovePhoneNumber()
        {
            var result = await repo.UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            var user = await repo.UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return Ok(new { Message = "RemovePhoneSuccess" });
        }

        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }
            var result = await repo.UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await repo.UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return Ok(new { Message = "ChangePasswordSuccess" });
            }
            return StatusCode(HttpStatusCode.NotFound);
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            var authentication = Request.GetOwinContext().Authentication;
            authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            authentication.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(repo.UserManager));
        }
    }
}
