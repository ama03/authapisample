using System.Web.Http;
using AddinX.Logging;

namespace Auth.Sample.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    { 
        protected BaseApiController(ILogger logger)
        {
            Logger = logger;
        }
        
        protected ILogger Logger { get; }
    }
}