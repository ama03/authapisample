﻿using System.Web.Http;
using AddinX.Configuration.Contract;
using AddinX.Logging;
using Auth.Sample.Api.Models;

namespace Auth.Sample.Api.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : BaseApiController
    {
        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(OrderModel.CreateOrders());
        }

        public OrdersController(ILogger logger)
            : base(logger) { }
    }
}
