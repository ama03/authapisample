﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AddinX.Configuration.Contract;

namespace Auth.Sample.Api
{
    public class ApiConfig
    {
        private readonly IConfigurationManager config;

        public ApiConfig( IConfigurationManager configurationManager)
        {
            config = configurationManager;
        }

        public string GoogleClientId => config.AppSettings["ggl:ClientId"];

        public string GoogleClientSecret => config.AppSettings["ggl:ClientSecret"];

        public string FacebookAppId => config.AppSettings["fb:AppId"];

        public string FacebookAppSecret => config.AppSettings["fb:AppSecret"];

        public string FacebookAppToken => config.AppSettings["fb:AppToken"];

        public string JwtIssuer=> config.AppSettings["jwt:Issuer"];

        public string JwtAudienceSecret => config.AppSettings["jwt:AudienceSecret"];

        public string JwtAudienceId => config.AppSettings["jwt:AudienceId"];
    }
}