﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.Jwt;

[assembly: OwinStartup(typeof(Auth.Sample.Api.Startup))]

namespace Auth.Sample.Api
{
    public partial class Startup
    {
        public static JwtBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static GoogleOAuth2AuthenticationOptions GoogleAuthOptions { get; private set; }
        public static FacebookAuthenticationOptions FacebookAuthOptions { get; private set; }
        
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            ContainerConfig.Register(config);

            ConfigureOAuth(app);
            
            WebApiConfig.Register(config);
            
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            config.Filters.Add(new AuthorizeAttribute());
        
            //Database.SetInitializer(
            //    new MigrateDatabaseToLatestVersion<AuthContext, Migrations.Configuration>());
        }
    }
}