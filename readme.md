##  web.config file

### AppSettings

You will need to update the value of the following key with information provided by Google and Facebook:

* fb:AppId
* fb:AppSecret
* fb:AppToken
* ggl:ClientId
* ggl:ClientSecret

To get the information, you need to connect to the following URL and create an account

* Facebook: https://developers.facebook.com
 * App Token : https://developers.facebook.com/tools/accesstoken/
* Google: https://console.developers.google.com


### Connection String

Define the *username* and *password* for the connection string to a local database. The migration script contains in the startup.cs will generate the database. The connection string must be name **AuthContext”**.

```csharp
<add name="AuthContext" connectionString="server=localhost; database=AngularIdentity;uid=(userName);password=(password);" providerName="System.Data.SqlClient" />
```

## Migration script

You can activate the migration script by removing the comments of the code below contained in the file *Startup.cs*

```csharp
  Database.SetInitializer(
     new MigrateDatabaseToLatestVersion<AuthContext, Migrations.Configuration>());
```
